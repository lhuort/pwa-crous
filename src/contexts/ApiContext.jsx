import { createContext, useContext, useEffect, useReducer } from "react";
import image1 from "../assets/images/antinea/img1.jpg";
import image2 from "../assets/images/antinea/img2.jpg";
import image3 from "../assets/images/antinea/img3.jpg";

const ApiContext = createContext();

const ApiProvider = ({ children }) => {
  const initialState = {
    restaurants: [],
    dishOfTheDay: null,
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case "restaurants": {
        return { ...state, restaurants: action.payload };
      }

      case "dishOfTheDay": {
        return { ...state, dishOfTheDay: action.payload };
      }

      default: {
        return state;
      }
    }
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  const setRestaurants = async () => {
    const restaurants = [
      {
        name: "Antinéa",
        rating: await fetch(
          "https://bbessere.lpmiaw.univ-lr.fr/resto.php?getrating=true"
        ).then((response) => response.json()),
        image: image1,
      },
      {
        name: "République",
        rating: await fetch(
          "https://bbessere.lpmiaw.univ-lr.fr/resto.php?getrating=true"
        ).then((response) => response.json()),
        image: image2,
      },
      {
        name: "Vespucci",
        rating: await fetch(
          "https://bbessere.lpmiaw.univ-lr.fr/resto.php?getrating=true"
        ).then((response) => response.json()),
        image: image3,
      },
    ];

    dispatch({
      type: "restaurants",
      payload: restaurants,
    });
  };

  const loadDishOfTheDay = async () => {
    const dishOfTheDay = await fetch(
      "https://bbessere.lpmiaw.univ-lr.fr/resto.php?getsuggestion=true"
    ).then((response) => {
      return response.text();
    });

    dispatch({
      type: "dishOfTheDay",
      payload: dishOfTheDay,
    });
  };

  useEffect(() => {
    setRestaurants();
  }, []);

  const bookReservation = async (date, time, people) => {
    // DD/MM/YYYY HH:MM
    if (date.toDate) {
      date = date.toDate();
    }
    if (time.toDate) {
      time = time.toDate();
    }
    const dateFormatted = `${date.getDate()}/${
      date.getMonth() + 1
    }/${date.getFullYear()}`;
    const timeFormatted = `${time.getHours()}/${time.getMinutes()}/${
      time.getSeconds() + 1
    }`;

    const response = await fetch(
      `https://bbessere.lpmiaw.univ-lr.fr/resto.php`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          date: dateFormatted,
          time: timeFormatted,
          amount: people,
          name: "Louis HUORT",
        }),
      }
    ).then((response) => response.text());

    return response;
  };

  return (
    <ApiContext.Provider
      value={{
        state,
        dispatch,
        loadDishOfTheDay,
        bookReservation,
      }}
    >
      {children}
    </ApiContext.Provider>
  );
};

const useApi = () => {
  if (!useContext(ApiContext)) {
    throw new Error("useApi must be used within ApiProvider");
  }

  return useContext(ApiContext);
};

export { ApiProvider, useApi };
