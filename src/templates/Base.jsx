import { Box, createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import BottomNavigation from "../components/BottomNavigation";
import { ApiProvider } from "../contexts/ApiContext";
import { NavigationProvider } from "../contexts/NavigationContext";
import Index from "../pages/Index";
import SwipeHandler from "./Swipehandler";

const theme = createTheme({
  palette: {
    mode: "dark",
  },
});

const Base = ({ children }) => {
  // Get restaurant from query string
  const restaurant = new URLSearchParams(window.location.search).get(
    "restaurant"
  );

  return (
    <NavigationProvider>
      <ThemeProvider theme={theme}>
        <ApiProvider>
          <CssBaseline />
          <Box
            sx={{
              display: "flex",
              width: "100%",
              flexDirection: "column",
              height: "100%",
              bgcolor: "background.default",
              color: "text.primary",
            }}
          >
            <SwipeHandler>
              {children.map((children, i) => (
                <section
                  key={`page-section-${i}`}
                  style={{
                    height: "100%",
                  }}
                >
                  {children}
                </section>
              ))}
            </SwipeHandler>
            <BottomNavigation />
            <Index visible={!restaurant} />
          </Box>
        </ApiProvider>
      </ThemeProvider>
    </NavigationProvider>
  );
};

export default Base;
