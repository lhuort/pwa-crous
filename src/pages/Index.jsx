import { Card, List, ListItem, Modal, Rating } from "@mui/material";
import Typography from "@mui/material/Typography";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import { Box } from "@mui/system";
import { useApi } from "../contexts/ApiContext";

const Index = ({ visible }) => {
  const {
    state: { restaurants },
  } = useApi();

  return (
    <Modal open={visible}>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          gap: 3,
          paddingTop: 2,
          paddingBottom: 2,
          height: "100%",
          width: "100%",
          position: "absolute",
          top: 0,
          left: 0,
          backgroundColor: "background.default",
        }}
      >
        <Typography as={"h1"} variant={"h4"}>
          Choisir un restaurant
        </Typography>
        <List
          sx={{
            display: "flex",
            flexDirection: "column",
            gap: 3,
            width: "100%",
          }}
        >
          {restaurants.map((restaurant, i) => (
            <ListItem
              key={`restaurant-${i}`}
              onClick={() => {
                if (restaurant.name === "Antinéa") {
                  window.location.href = `${
                    process.env.PUBLIC_URL ?? ""
                  }/carte?restaurant=${restaurant.name}`;
                }
              }}
            >
              <Card
                sx={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  gap: 2,
                  padding: 2,
                }}
              >
                <Box
                  sx={{
                    width: 100,
                  }}
                >
                  <img
                    src={restaurant.image}
                    alt={restaurant.name}
                    style={{
                      width: "100%",
                    }}
                  />
                </Box>
                <Typography as={"h2"} variant={"h6"}>
                  {restaurant.name}
                </Typography>
                <Rating value={restaurant.rating} />
                <ArrowForwardIosIcon />
              </Card>
            </ListItem>
          ))}
        </List>
      </Box>
    </Modal>
  );
};

export default Index;
