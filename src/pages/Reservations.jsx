import {
  Button,
  Container,
  FormControl,
  FormGroup,
  Slider,
  TextField,
  Typography,
} from "@mui/material";
import { DatePicker, TimePicker } from "@mui/x-date-pickers";
import { useState } from "react";
import { toast } from "react-toastify";
import { useApi } from "../contexts/ApiContext";
import localforage from "localforage";

const Reservations = () => {
  const [dateValue, setDateValue] = useState(new Date());
  const [timeValue, setTimeValue] = useState(new Date());
  const [numberOfPeople, setNumberOfPeople] = useState(2);
  const { bookReservation } = useApi();

  const handleToast = (message, severity) => {
    if (severity === "success") {
      toast.success(message);
    } else {
      toast.error(message);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const id = new Date().getTime();

    // Add reservation to the database
    localforage.getItem("reservations").then((reservations) => {
      if (reservations) {
        reservations.push({
          date: dateValue,
          time: timeValue,
          numberOfPeople: numberOfPeople,
          id: id,
        });
        localforage.setItem("reservations", reservations);
      } else {
        localforage.setItem("reservations", [
          {
            date: dateValue,
            time: timeValue,
            numberOfPeople: numberOfPeople,
            id: id,
          },
        ]);
      }
    });

    bookReservation(dateValue, timeValue, numberOfPeople)
      .then((response) => {
        handleToast("Réservation prise en compte", "success");
        // Then delete the reservation from the database
        localforage.getItem("reservations").then((reservations) => {
          if (reservations) {
            reservations = reservations.filter(
              (reservation) => reservation.id !== id
            );
            localforage.setItem("reservations", reservations);
          }
        });
      })
      .catch((error) => {
        // handleToast("Une erreur est survenue", "error");
      });
  };

  return (
    <Container
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        gap: 3,
        paddingTop: 2,
        paddingBottom: 2,
        height: "100%",
      }}
    >
      <Typography as={"h1"} variant={"h4"}>
        Réservation
      </Typography>
      <FormControl
        sx={{
          display: "flex",
          flexDirection: "column",
          gap: 3,
          width: "100%",
        }}
        as="form"
        onSubmit={handleSubmit}
      >
        <DatePicker
          label="Date"
          value={dateValue}
          onChange={(newValue) => {
            setDateValue(newValue);
          }}
          renderInput={(params) => <TextField {...params} />}
        />
        <TimePicker
          label="Heure"
          value={timeValue}
          onChange={(newValue) => {
            setTimeValue(newValue);
          }}
          renderInput={(params) => <TextField {...params} />}
        />
        <FormGroup>
          <Typography>Nombre de personnes: {numberOfPeople}</Typography>
          <Slider
            value={numberOfPeople}
            onChange={(event, newValue) => {
              setNumberOfPeople(newValue);
            }}
            className="no-swipe"
            valueLabelDisplay="off"
            marks
            min={1}
            max={10}
          />
        </FormGroup>
        <Button
          variant="contained"
          sx={{
            alignSelf: "center",
          }}
          type="submit"
        >
          Réserver
        </Button>
      </FormControl>
    </Container>
  );
};

export default Reservations;
